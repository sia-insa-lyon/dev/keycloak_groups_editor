package fr.bdeinsalyon.sia.keycloak_groups_editor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@SpringBootApplication() //exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class}
public class KeycloakGroupsEditorApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeycloakGroupsEditorApplication.class, args);
	}
}
