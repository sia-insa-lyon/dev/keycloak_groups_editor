package fr.bdeinsalyon.sia.keycloak_groups_editor.views.groups;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Section;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.*;
import fr.bdeinsalyon.sia.keycloak_groups_editor.KeycloakAdminService;
import fr.bdeinsalyon.sia.keycloak_groups_editor.views.main.MainView;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static fr.bdeinsalyon.sia.keycloak_groups_editor.views.Utils.getAccessToken;

@CssImport("./styles/views/groups/edit.css")
@PageTitle("Éditer un groupe")
@Route(value = "edit-group", layout = MainView.class)
public class EditGroupView extends Section implements HasUrlParameter<String>, UserPickView.OnUserChosenListener {
    private KeycloakAdminService kas;
    Grid<UserRepresentation> groupMembersGrid = new Grid<>();
    Button deleteSelected = new Button("Retirer les membres sélectionnés du groupe");
    //private Div userDiv = new Div();
    private GroupRepresentation group;

    public EditGroupView(@Autowired KeycloakAdminService kas) {
        this.kas = kas;
        //add(userDiv);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, @WildcardParameter String groupId) {
        List<String> tokenUserGroups = (List<String>) getAccessToken().getOtherClaims().get("groups");
        group = kas.getRealm().groups().group(groupId).toRepresentation();
        String groupPath = group.getPath();

        /*
         * si c'est le groupe bureau, on check juste que le mec est dedans
         * si c'est un groupe asso, on vérifie qu'il est dans le sous_groupe bureau
         */
        if (kas.isBureauGroup(group)) {
            if (tokenUserGroups.contains(group.getPath()))
                createView(groupPath);
        } else {
            if (tokenUserGroups.contains(kas.getBureauSubgroup(group).getPath()))
                createView(groupPath);
            else
                add("Vous n'avez pas la permission d'éditer ce groupe. Si c'est un groupe d'asso, vous devez être dans le sous-groupe Bureau de cette" +
                        "asso pour l'éditer.");
        }
        /*
        if ((kas.isBureauGroup(group) && tokenUserGroups.contains(group.getPath())) || tokenUserGroups.contains(kas.getBureauSubgroup(group).getPath())) {
            createView(groupPath);
        } else {
            add("Vous n'avez pas la permission d'éditer ce groupe. Si c'est un groupe d'asso, vous devez être dans le sous-groupe Bureau de cette" +
                    "asso pour l'éditer.");
        }*/
    }

    @Override
    public void userChosen(UserRepresentation user) {
        /*userDiv.removeAll();
        userDiv.add(new Text("utilisateur ajouté: " + user.getEmail()));*/
        kas.addUserToGroup(user, group);
        refreshGroupMembers();
    }

    public Grid<UserRepresentation> createGrid() {
        this.groupMembersGrid.addColumn(UserRepresentation::getFirstName).setHeader("Prénom");
        this.groupMembersGrid.addColumn(UserRepresentation::getLastName).setHeader("Nom");
        this.groupMembersGrid.addColumn(UserRepresentation::getEmail).setHeader("E-mail");
        this.groupMembersGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        this.groupMembersGrid.addSelectionListener(selectionEvent -> {
        });
        return this.groupMembersGrid;
    }

    public List<UserRepresentation> refreshGroupMembers() {
        List<UserRepresentation> uig = kas.usersInGroup(group);
        groupMembersGrid.setItems(uig);
        return uig;
    }

    private void createView(String groupPath) {
        setTitle("Édition de " + groupPath);
        //add("param :" + groupPath);
        add(new UserPickView(kas, this, "Ajouter"));

        add(new Hr());

        HorizontalLayout hl = new HorizontalLayout();
        hl.setVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        hl.add(new H4("Membres du groupe " + groupPath));
        hl.add(deleteSelected);
        add(hl);

        deleteSelected.getElement().setAttribute("theme", "error");
        deleteSelected.addClickListener(buttonClickEvent -> {
            kas.removeUsersFromGroup(this.groupMembersGrid.getSelectedItems(), group);
            refreshGroupMembers();
        });

        add(createGrid());
        refreshGroupMembers();
    }
}
