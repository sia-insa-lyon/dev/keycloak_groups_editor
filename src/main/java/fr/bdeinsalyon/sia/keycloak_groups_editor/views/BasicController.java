package fr.bdeinsalyon.sia.keycloak_groups_editor.views;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Controller
public class BasicController {
    @GetMapping("/")
    public String index(@Autowired HttpServletRequest request) {
        if (request.getUserPrincipal() != null)
            return "loggedin";
        else
            return "index";
    }

    @GetMapping("/connexion")
    public RedirectView connexion(@Autowired HttpServletRequest request) {
        return new RedirectView("/app/");
    }

    @GetMapping("/deconnexion")
    public RedirectView deconnexion(@Autowired HttpServletRequest request) {
        try {
            request.logout();
        } catch (ServletException e) {
            e.printStackTrace();
        }
        return new RedirectView("/");
    }

    @GetMapping("/me")
    public String me(@Autowired HttpServletRequest request, Model model) {
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) request.getUserPrincipal();
        KeycloakPrincipal principal = (KeycloakPrincipal) token.getPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        AccessToken accessToken = session.getToken();
        Set<String> clientRoles = accessToken.getResourceAccess(System.getenv("KEYCLOAK_CLIENT_ID")).getRoles();
        AccessToken.Access realmAccess = accessToken.getRealmAccess();
        Set<String> roles = realmAccess.getRoles();

        List<String> groups = (List<String>) accessToken.getOtherClaims().get("groups");
        if (groups == null) { // le Client Keycloak est mal configuré, il manque un mapper pour les groupes
            groups = new ArrayList<>(Collections.singleton("LE CLIENT KEYCLOAK EST MAL CONFIGURÉ !!" +
                    " Il manque le Mapper 'groups' (sur l'AccessToken)"));
        }
        roles.addAll(clientRoles);

        model.addAttribute("username", accessToken.getPreferredUsername());
        model.addAttribute("email", accessToken.getEmail());
        model.addAttribute("lastname", accessToken.getFamilyName());
        model.addAttribute("firstname", accessToken.getGivenName());
        model.addAttribute("realmName", accessToken.getIssuer());
        model.addAttribute("roles", roles.toArray());
        model.addAttribute("groups", groups);
        return "userinfo";
    }

    @GetMapping("/test")
    public String test(){
        return "index";
    }
}
