package fr.bdeinsalyon.sia.keycloak_groups_editor.views.groups;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import fr.bdeinsalyon.sia.keycloak_groups_editor.KeycloakAdminService;
import fr.bdeinsalyon.sia.keycloak_groups_editor.views.main.MainView;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.beans.factory.annotation.Autowired;

import static fr.bdeinsalyon.sia.keycloak_groups_editor.views.Utils.getAccessToken;

@Route(value = "my-groups", layout = MainView.class)
@PageTitle("Mes groupes SSO")
public class MyGroupsView extends VerticalLayout {
    public MyGroupsView(@Autowired KeycloakAdminService kas) {
        AccessToken accessToken = getAccessToken();


        UnorderedList groupsListView = new UnorderedList();

        //TODO: mutualiser la gestion des droits avec EditGroupView dans KAS pour afficher uniquement les groupes administrables
        for (GroupRepresentation group : kas.getRealm().users().get(accessToken.getSubject()).groups()) {
                Button editButton = new Button("Éditer");
                editButton.addClickListener(buttonClickEvent -> {
                    getUI().ifPresent(ui -> ui.navigate(EditGroupView.class, group.getId()));
                });
                groupsListView.add(new ListItem(new Text(group.getPath().replace("/", " / ")), editButton));
        }
        add(groupsListView);
    }
}
