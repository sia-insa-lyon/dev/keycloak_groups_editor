package fr.bdeinsalyon.sia.keycloak_groups_editor.views.groups;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.H6;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import fr.bdeinsalyon.sia.keycloak_groups_editor.KeycloakAdminService;
import fr.bdeinsalyon.sia.keycloak_groups_editor.views.main.MainView;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static fr.bdeinsalyon.sia.keycloak_groups_editor.views.Utils.getAccessToken;

@CssImport("./styles/views/groups/list.css")
@Route(value = "groups/list", layout = MainView.class)
@PageTitle("Liste des groupes Keycloak")
public class GroupsListView extends VerticalLayout {

    public GroupsListView(@Autowired KeycloakAdminService kca) { //il faut autowire dans le constructeur pour Vaadin
        // sinon la DI se fait après le constructeur, donc kca est vide
        add(new Paragraph("Voici la liste de tous les groupes du SSO Asso-insa-lyon (Keycloak). Pour modifier des groupes, allez voir la page 'Mes Groupes'"));

        HorizontalLayout dyn = new HorizontalLayout();
        dyn.setVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        add(dyn);

        List<GroupRepresentation> groupRepresentationList = kca.listGroups();

        TreeGrid<GroupRepresentation> treeGridGroups = new TreeGrid<>();
        treeGridGroups.setItems(groupRepresentationList, GroupRepresentation::getSubGroups);
        treeGridGroups.addHierarchyColumn(GroupRepresentation::getName).setHeader("Groupes");

        treeGridGroups.expandRecursively(groupRepresentationList, 1);

        add(treeGridGroups);

        AccessToken accessToken = getAccessToken();

        treeGridGroups.addItemDoubleClickListener(groupRepresentationItemDoubleClickEvent -> {
            dyn.removeAll();
            if (((List<String>) accessToken.getOtherClaims().get("groups")).contains(groupRepresentationItemDoubleClickEvent.getItem().getPath())) {
                dyn.add(new H6(groupRepresentationItemDoubleClickEvent.getItem().getPath()));
                dyn.add(new RouterLink(" Éditer", EditGroupView.class, groupRepresentationItemDoubleClickEvent.getItem().getId()));
            } else {
                dyn.add(new Text("Erreur: vous n'avez pas la permission de modifier ce groupe !"));
            }
        });
    }
}
