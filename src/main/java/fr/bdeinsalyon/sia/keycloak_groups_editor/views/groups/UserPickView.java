package fr.bdeinsalyon.sia.keycloak_groups_editor.views.groups;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import fr.bdeinsalyon.sia.keycloak_groups_editor.KeycloakAdminService;
import org.keycloak.representations.idm.UserRepresentation;

//@Tag("div")
public class UserPickView extends VerticalLayout {
    private UnorderedList usersList = new UnorderedList();
    private KeycloakAdminService keycloakAdminService;
    private OnUserChosenListener chosenListener;
    TextField searchIn = new TextField();
    private String chooseButtonText = "Sélectionner";

    public UserPickView(KeycloakAdminService kas, OnUserChosenListener cl, String chooseButtonText) {
        this.chooseButtonText = chooseButtonText;
        this.keycloakAdminService = kas;
        this.chosenListener = cl;
        add(new H5("Recherche"));

        HorizontalLayout searchbar = new HorizontalLayout();
        Button searchBut = new Button("Recherche");
        searchIn.setValueChangeMode(ValueChangeMode.ON_CHANGE);
        searchIn.addValueChangeListener(textFieldStringComponentValueChangeEvent -> search());
        searchBut.addClickListener(buttonClickEvent -> search());
        searchbar.add(searchIn, searchBut);
        add(searchbar);
        add(usersList);
    }

    private void search() {
        String s = searchIn.getValue();
        usersList.removeAll();
        for (UserRepresentation userRepresentation : keycloakAdminService.searchUser(s)) {
            Button choose = new Button(chooseButtonText);
            choose.addClickListener(buttonClickEvent -> {
                chosenListener.userChosen(userRepresentation);
            });
            usersList.add(new ListItem(new Text(userRepresentation.getEmail()), new Text(" "), choose));
        }
    }

    public interface OnUserChosenListener {
        public void userChosen(UserRepresentation user);
    }
}
