package fr.bdeinsalyon.sia.keycloak_groups_editor.views.main;


import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Accueil")
@Route(value = "child",layout = MainView.class)
public class MainChildView extends VerticalLayout {
    public MainChildView(){
        setId("child-view");
        add(new Text("Bonjour"));
    }
}
