package fr.bdeinsalyon.sia.keycloak_groups_editor;


import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "keycloak", name = "enabled", havingValue = "true")
public class KeycloakAdminService {
    private Logger logger = LoggerFactory.getLogger(KeycloakAdminService.class);
    private String realm;
    private Keycloak keycloak = null;

    public KeycloakAdminService() {
        String url = System.getenv("KEYCLOAK_URL");
        realm = System.getenv("KEYCLOAK_REALM");
        String clientId = System.getenv("KEYCLOAK_CLIENT_ID");
        String clientSecret = System.getenv("KEYCLOAK_CLIENT_SECRET");

        logger.info("Connexion à Keycloak");
        try {
            keycloak = KeycloakBuilder.builder()
                    .serverUrl(url)
                    .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                    .realm(realm)
                    .clientId(clientId)
                    .clientSecret(clientSecret)
                    .build();

            keycloak.tokenManager().getAccessToken();
        } catch (javax.ws.rs.ProcessingException e) {
            logger.error("Impossible de se connecter à Keycloak", e);
        }
    }

    public void exploreGroups() {
        List<GroupRepresentation> l = keycloak.realm("asso-insa-lyon").groups().groups();
        for (GroupRepresentation group : l) {
            System.out.println(group.getName());
            for (GroupRepresentation g : group.getSubGroups()) {
                System.out.println(g.getName());
            }
        }
    }

    public RealmResource getRealm() {
        return keycloak.realm(realm);
    }

    public List<GroupRepresentation> listGroups() {
        if (keycloak != null)
            return getRealm().groups().groups();
        else {
            logger.warn("keycloak static is null !!");
            return new ArrayList<>();
        }
    }

    public List<UserRepresentation> searchUser(String s) {
        return getRealm().users().search(s, 0, 10); //force la recherche pas n'importe quel champ
    }

    public GroupRepresentation getGroupByPath(String path) {
        return getRealm().getGroupByPath(path);
    }

    public List<UserRepresentation> usersInGroup(String groupPath) {
        return usersInGroup(getGroupByPath(groupPath));
        /*String[] hierarchy = groupPath.split("/");
        GroupRepresentation group =
        for (int i = 1; i < hierarchy.length; i++) {

        }*/
    }

    public List<UserRepresentation> usersInGroup(GroupRepresentation group) {
        return getRealm().groups().group(group.getId()).members();
        /*String[] hierarchy = groupPath.split("/");
        GroupRepresentation group =
        for (int i = 1; i < hierarchy.length; i++) {

        }*/
    }

    /**
     * Retire un utilisateur d'un groupe, et ça le retire aussi du Bureau s'il y a lieu.
     * Fait en sorte de ne pas laisser les gens dans le bureau de l'asso s'ils ne sont même plus adhérents.
     *
     * @param user  utilisateur
     * @param group group
     */
    public void removeUserFromGroup(UserRepresentation user, GroupRepresentation group) {
        getRealm().users().get(user.getId()).leaveGroup(group.getId());
        if (hasBureauSubgroup(group)) {
            removeUserFromGroup(user, getBureauSubgroup(group));
        }
    }

    /**
     * Ajoute l'utilisateur au groupe
     *
     * @param user  user
     * @param group group
     */
    public void addUserToGroup(UserRepresentation user, GroupRepresentation group) {
        if (isBureauGroup(group)) {
            GroupRepresentation parentGroup = getParentGroup(group);
            if (parentGroup != null) {
                addUserToGroup(user, parentGroup);
            } else {
                logger.warn("parent group is null ! not expected for a Bureau group");
            }
        }
        // il faut le faire dans cet ordre, car dans keycloak, on ne peut pas rajouter quelqu'un à un groupe parent
        getRealm().users().get(user.getId()).joinGroup(group.getId());
    }

    public void removeUsersFromGroup(Collection<UserRepresentation> users, GroupRepresentation group) {
        for (UserRepresentation user : users) {
            removeUserFromGroup(user, group);
        }
    }

    /*
    regarde s'il y a un attribut Bureau=true
     */
    public boolean isBureauGroup(GroupRepresentation group) {
        if (group.getAttributes().containsKey("bureau")) {
            return group.getAttributes().get("bureau").get(0).equals("true");
        } else
            return false;
    }

    public boolean isAssoGroup(GroupRepresentation group) {
        if (group.getAttributes().containsKey("bureau") && group.getAttributes().containsKey("asso")) {
            return group.getAttributes().get("bureau").get(0).equals("false");
        } else
            return false;
    }

    public boolean hasBureauSubgroup(GroupRepresentation group) {
        for (GroupRepresentation subgroup : group.getSubGroups()) {
            if (isBureauGroup(subgroup)) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    public GroupRepresentation getBureauSubgroup(GroupRepresentation group) {
        for (GroupRepresentation subgroup : group.getSubGroups()) {
            if (isBureauGroup(subgroup)) {
                return subgroup;
            }
        }
        return null;
    }

    @Nullable
    public GroupRepresentation getParentGroup(GroupRepresentation group) {
        String[] path = group.getPath().split("/");
        if (path.length > 2) {
            StringBuilder parentGroupPath = new StringBuilder();
            for (int i = 1; i < path.length - 1; i++) {
                parentGroupPath.append("/").append(path[i]);
            }
            return getRealm().getGroupByPath(parentGroupPath.toString());
        }
        return null;
    }

    public boolean canAdministrate(GroupRepresentation group, UserRepresentation user) {
        if (isBureauGroup(group))
            return true;
        else {
        }
        return false;
    }
}
