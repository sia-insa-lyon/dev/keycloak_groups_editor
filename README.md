# Configuration
## Keycloak
* Créer un client avec des ServiceAccount (voir les groupes), et lui rajouter
un rôle ``assoprez``.
* Rajouter un mapper de type *Group Membership* (ne pas utiliser les builtins, ils vont mettre des rôles, jsp pk), avec comme *Token Claim Name* `groups`, en cochant bien *"Full group path"*
* Donnez en ServiceAccount Roles `realm-management/query-users`
### Configuration des groupes
Les groupes dont la gestion est déléguée aux associations doivent
avoir l'attribut `asso=true` ainsi que `bureau=false`.
Les sous-groupes bureau(être dedans donne la permission d'éditer les groupes de l'asso)
doivent avoir l'attribut `bureau=true`.

Les groupes **Bureau** doivent avoir le *Role* `assoprez` pour que les membres du bureau aient
accès à l'appli. On peut aussi leur assigner le rôle à la main, mais c'est moins pratique ...
## Variables d'environnement
```
KEYCLOAK_ENABLED=true #pour désactiver la connexion dans les tests
KEYCLOAK_CLIENT_SECRET=3207c33e-8b1c-464e-b11a-c5c43194cdbd
KEYCLOAK_CLIENT_ID=groups-manager
KEYCLOAK_REALM=asso-insa-lyon
KEYCLOAK_URL=http://localhost:8080/auth/
PORT=9090
```
## Déploiement
    Ce serveur n'a pas besoin de base de données.
# Aide
Le HTML se fait dans les Views Vaadin. Regarder MainView.java#createMenuItems() pour voir comment ajouter des pages au drawer (il faut aussi mettre @Route("/..", layout = MainView.class) pour avoir l'UI autour)

Pour rajouter des ressource statiques (css, images), il faut les mettre dans le dossier `frontend`, puis les importer.
> @JsModule("./styles/shared-styles.js")

(`./` correspond au dossier ``frontend``)

## @Autowired et @Bean
Ce sont des annotations Java pour faire de la Dependency Injection.
Ça veut dire que les classes reçoivent leurs attributs au lieu de les créer eux-même.
Par contre, ça ne marche que quand Spring gère les objets (il ne devrait pas y avoir de problèmes tant qu'on fait pas de trucs chelou); et aussi les attributs ne sont pas disponibles dans le constructeur (il y a des méthodes post-init dispo).
# Bugs
## Comment les prévenir
* ne pas oublier @PageTitle sur les vues Vaadin
* attention, les groupes
Les logs se plaignent qu'il ne peuvent pas déconnecter l'utilisateur, mais ça marche.
```
 Cannot log out a non-Keycloak authentication: org.springframework.security.authentication.AnonymousAuthenticationToken@e80a0
```
## Gotchas
* On ne peut pas ajouter qqn à un groupe s'il est déjà dans un de ses sous-groupes.